package com.company;

public class Main {

    public static void main(String[] args) {

        /**
         * Щомісячна капіталізація
         * Коли банк підсумовує прибуток за депозитом раз на місяць, розрахунок ведеться за формулою:
         * Дх = Р * (1 + (N/12)), зведене в ступінь Т)
         * де:
         * Дх — підсумковий дохід, тобто розмір вкладу на кінець терміну, включаючи початкову суму та нараховані проценти
         * P - початковий депозит
         * N - річна ставка, поділена на 100
         * T - термін договору в місяцях
         *
         * Процентний дохід визначається як різниця накопиченої суми вкладу та початкової
         */

        double amount;
        double rate;
        double term;
        final int MONTHS_PER_YEAR = 12;
        double t;
        double interest = 0;

        System.out.println("Java Deposit Calculator");
        System.out.println();
        System.out.print("Enter the deposit amount, UAH: " + args[0]);
        System.out.println();
        System.out.print("Enter the annual interest rate, %: " + args[1]);
        System.out.println();
        System.out.print("Enter the deposit term, years: " + args[2]);
        System.out.println();

        amount = Double.parseDouble(args[0]);
        rate = Double.parseDouble(args[1]);
        term = Double.parseDouble(args[2]);
        t = term / term * MONTHS_PER_YEAR;
        System.out.println();
        System.out.println("-----");

        for (int i = 1; i <= term; i++) {
            interest = amount * Math.pow((1 + ((rate / 100) / 12)), t);
            amount += interest;
            System.out.println("The income interest for the " + i + " year: " + interest + " UAH");
            System.out.println("The accumulated amount for the " + i + " year: " + amount + " UAH");
            System.out.println();
            System.out.println("-----");
        }

    }
}